const { model, Schema } = require('mongoose');
ObjectId = Schema.ObjectId;

// create chat table schema
const chatSchema = new Schema({
  senderId: { type: ObjectId },
  receiverId: { type: ObjectId },
  messageContent: { type: String },
  createdAt: { type: Date },
  mediaUploaded: { type: Boolean },
});

const ChatModel = model('chats', chatSchema);

module.exports = ChatModel;
