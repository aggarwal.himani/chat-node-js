const { model, Schema } = require('mongoose');
ObjectId = Schema.ObjectId;

// create chatMedia table schema having chat id and associated media
const chatmediaSchema = new Schema({
  chatId: { type: ObjectId },
  media: { type: String },
  createdAt: { type: Date, default: Date.now },
});

const ChatmediaModel = model('chatMedia', chatmediaSchema);

module.exports = ChatmediaModel;
