const { model, Schema } = require('mongoose');

// create user table schema
const userSchema = new Schema({
  username: String,
  password: String,
  name: String,
  profile_pic: String,
});

const UserModel = model('users', userSchema);

module.exports = UserModel;
