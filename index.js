const express = require('express'); // Include ExpressJS
const bodyParser = require('body-parser'); // Middleware
const app = express(); // Create an ExpressJS app
const port = 5000; // Port we will listen on
const initMongo = require('./utils/mongoose-init.js');
const passport = require('passport');
var session = require('express-session');
const socket = require('socket.io');
const bcrypt = require('bcryptjs');
const { authRouter, verifyToken } = require('./modules/Auth');
const chatRouter = require('./modules/Chat');
const { AuthError, SystemError, ValidationError } = require('./utils/errors');
var path = require('path');
const cookieParser = require('cookie-parser');

// Function to listen on the port
const server = app.listen(port, () => {
  console.log(`listening at port ${port}`);
});

let io = require('socket.io')({
  allowEIO3: true, // false by default
}).listen(server);

io.on('connection', function (socket) {
  console.log('Made socket connection');

  socket.on('chat message', function (data) {
    io.emit('chat message', data);
  });
  socket.on('typing', function (data) {
    socket.broadcast.emit('typing', data);
  });
});
initMongo();

app.use(cookieParser());
app.set('view engine', 'ejs');

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use(express.static(path.resolve('./public'))); // Static files
app.use(
  session({
    secret: '123456catr',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000 },
  })
);

//=====================
// ROUTES
//=====================
// Showing register form

app.get('/', (req, res) => {
  res.render('index', { title: 'Register' });
});

app.use('/auth', authRouter); // here we want express to use authRouter for all requests coming at /auth like /auth/login
app.use((req, res, next) => {
  const token = req.cookies['nToken']; //fetch access token from cookies
  if (typeof token === 'string' && token) {
    const user = verifyToken(token.replace('Bearer ', ''));
    req.user = user;
    next();
  } else {
    throw new AuthError('token not provided');
  }
});

app.use('/messages', chatRouter); // here we want express to use chatRouter for all requests coming at /messages
app.use((err, req, res, next) => {
  const { statusCode, message } = err || {};
  if (statusCode < 500) {
    return res.status(statusCode).send(message);
  }
  const systemError = new SystemError();
  res.status(500).send(systemError.message);
});
