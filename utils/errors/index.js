module.exports = {
  AuthError: require('./AuthError.js'),
  SystemError: require('./SystemError.js'),
  ValidationError: require('./ValidationError.js'),
  DataError: require('./DataError.js'),
};
