class ValidationError {
  constructor(message) {
    this.statusCode = 400;
    this.message = message;
  }
}
module.exports = ValidationError;
