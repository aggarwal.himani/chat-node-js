class AuthError {
  constructor(message) {
    this.statusCode = 401; //set status code
    this.message = message; //set error message
  }
}
module.exports = AuthError;
