class DataError {
  constructor(message = 'Data not found') {
    this.statusCode = 404;
    this.message = message;
  }
}
module.exports = DataError;
