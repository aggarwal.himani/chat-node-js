class SystemError {
  constructor(message = 'Some error occurred') {
    this.statusCode = 500;
    this.message = message;
  }
}
module.exports = SystemError;
