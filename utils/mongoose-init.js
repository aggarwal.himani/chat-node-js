const mongoose = require('mongoose');

async function initMongo() {
  try {
    mongoose.connection
      .on('error', (err) => {
        console.error(err);
      })
      .on('open', (err) => {
        console.log(`DB connected`);
      });
      // Connecting to the database
    await mongoose.connect('mongodb+srv://himani01:himani01@flowapp.n1fku.mongodb.net/chatnode', {
      useNewUrlParser: true,
    });
  } catch (error) {
    console.error(error);
  }
}

module.exports = initMongo;
