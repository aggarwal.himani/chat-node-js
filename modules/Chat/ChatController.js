const { UserModel } = require('../../models');
const { ChatModel, ChatmediaModel } = require('../../models');
const { mongoose } = require('mongoose');
const { AuthError, SystemError, ValidationError, DataError } = require('../../utils/errors');

class ChatController {

  //fetch logged in user messages
  async receiverMessages(userId) {
    var users = await ChatModel.find({
      $or: [{ senderId: mongoose.Types.ObjectId(userId) }, { receiverId: mongoose.Types.ObjectId(userId) }],
    })
      .sort({ createdAt: -1 })
      .exec();
    return users;
  }

  //fetch messages with user selected
  async userMessages(receiverId, senderId) {
    const senderIdupdated = senderId.replace(':', '');
    const messages = await ChatModel.find({
      $or: [
        {
          receiverId: mongoose.Types.ObjectId(senderIdupdated),
          senderId: mongoose.Types.ObjectId(receiverId),
        },
        {
          receiverId: mongoose.Types.ObjectId(receiverId),
          senderId: mongoose.Types.ObjectId(senderIdupdated),
        },
      ],
    }).exec();
    return messages;
  }


    //fetch images attached to a particular chat id
  async fetchMedia(chatId) {
    const media = await ChatmediaModel.find({
      chatId: mongoose.Types.ObjectId(chatId),
    }).exec();
    return media;
  }

  //fetch latest message with a particular user
  async userlastMessage(receiverId, senderId) {
    const messages = await ChatModel.findOne({
      $or: [
        {
          senderId: mongoose.Types.ObjectId(receiverId),
          receiverId: mongoose.Types.ObjectId(senderId),
        },
        {
          receiverId: mongoose.Types.ObjectId(receiverId),
          senderId: mongoose.Types.ObjectId(senderId),
        },
      ],
    })
      .sort({ createdAt: -1 })
      .exec();
    return messages;
  }

//send new message
  async sendMessage(userId, message) {
    var newMessage = new ChatModel({
      senderId: userId,
      receiverId: message.senderId,
      messageContent: message.messageContent,
      createdAt: new Date(),
      mediaUploaded: message.mediaUploaded,
    });

    let saveMessage = await newMessage.save(); //save new message into database
    if (saveMessage) {
      const media = message.media;  //check if there is any attachment

     
      if (media.length != 0) {
        for (var y = 0; y < media.length; y++) {
          const filename = media[y].filename;
          var newMedia = new ChatmediaModel({
            chatId: saveMessage._id,
            media: filename,
            createdAt: new Date(),
          });

          let saveMedia = await newMedia.save(); //save attachment associated with chat id
        }
      }
    }

    return saveMessage;
  }

//fetch profile image of a user
  async fetchPic(userID) {
    const userInfo = await UserModel.findById(userID);
    return userInfo;
  }
}
module.exports = new ChatController();
