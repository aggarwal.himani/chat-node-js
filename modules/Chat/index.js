const { Router } = require('express');
const chatRouter = Router();
const UserController = require('../Auth/UserController.js');
const passport = require('passport');
const { createTokens, verifyToken } = require('../Auth/tokens');
const multer = require('multer');
const upload = multer({ dest: './public/data/uploads/' });
const ChatController = require('./ChatController.js');


//fetch chat messages
chatRouter.get('/', async (req, res) => {
  const data = await ChatController.receiverMessages(req.user._id);
  if (data) {
    var distinctUsers = [];
    for (var i = 0; i < data.length; i++) {
      const checkUser = data[i].senderId.toHexString();
      var anotherUserid = '';
      if (checkUser == req.user._id) {
        anotherUserid = data[i].receiverId;
      } else {
        anotherUserid = data[i].senderId;
      }
      const other = anotherUserid.toHexString();
      distinctUsers.push(other);
    }

    //fetch unique users from my chat list
    uniqueArray = distinctUsers.filter(function (elem, pos) {
      return distinctUsers.indexOf(elem) == pos;
    });

    var newArr = [];
    for (var y = 0; y < uniqueArray.length; y++) {
      var messages = {};
      const userMessages = await ChatController.userlastMessage(req.user._id, uniqueArray[y]);  //fetch last message with a user
      var formatArray = [];
      if (userMessages) {
        const mediaUploaded = userMessages.mediaUploaded;
        const chatId = userMessages._id.toHexString();
        var fetchMedia = [];
        if (mediaUploaded == 1) {  //check if there is ny attachment with a message
          fetchMedia = await ChatController.fetchMedia(chatId);
        }
        formatArray.push({ userMessages: userMessages, media: fetchMedia });
      }
      const userData = await ChatController.fetchPic(uniqueArray[y]); //fetch profile image of user

      var profilePic = '';
      if (userData) {
        profilePic = userData.profile_pic;
      } else {
        profilePic = '';
      }
      const userName = userData.name;
      newArr.push({
        user: uniqueArray[y],
        userPic: profilePic,
        userName: userName,
        msgArray: formatArray,
      });
    }

    res.render('messages', { messages: newArr, title: 'Messages' });
  } else {
    res.render('messages', { messages: [], title: 'Messages' });
  }
});


//fetch messages with a particular user
chatRouter.get('/user/:userId', async (req, res) => {
  const senderId = req.params.userId;
  const senderIdupdated = senderId.replace(':', '');
  const userMessages = await ChatController.userMessages(req.user._id, req.params.userId);
  var formatArray = [];
  if (userMessages) {
    for (var i = 0; i < userMessages.length; i++) {
      const mediaUploaded = userMessages[i].mediaUploaded;
      const chatId = userMessages[i]._id.toHexString();
      var fetchMedia = [];

      //check if there is ny attachment with a message
      if (mediaUploaded == 1) {
        fetchMedia = await ChatController.fetchMedia(chatId);
      }
      formatArray.push({ userMessages: userMessages[i], media: fetchMedia });
    }
  }
  res.render('userMessages', {
    messages: formatArray,
    senderId: senderIdupdated,
    senderName: req.user.username,
  });
});


//post a new message. Maximum images that can be uploaded is 12
chatRouter.post('/post', upload.array('media', 12), async (req, res, next) => {
  try {
    if (req.files) {
      req.body.mediaUploaded = true;
      req.body.media = req.files;
    } else {
      req.body.mediaUploaded = false;
      req.body.media = '';
    }
    const sendMessage = await ChatController.sendMessage(req.user._id, req.body);
    var obj = {};
    if (sendMessage) {
      const mediaUploaded = sendMessage.mediaUploaded;
      const chatId = sendMessage._id.toHexString();
      var fetchMedia = [];
      if (mediaUploaded == 1) {
        fetchMedia = await ChatController.fetchMedia(chatId);
      }
    }

    if (!Object.keys(obj).length) {
      Object.assign(obj, { message: sendMessage, media: fetchMedia });
    }
    return res.json(obj);
  } catch (err) {
    next(err);
  }
});
module.exports = chatRouter;
