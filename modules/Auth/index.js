const { Router } = require('express');
const authRouter = Router();
const UserController = require('./UserController.js');
const passport = require('passport');
const { createTokens, verifyToken } = require('./tokens');
const multer = require('multer');
const upload = multer({ dest: './public/data/uploads/' });
const bcrypt = require('bcryptjs');
const { ValidationError } = require('../../utils/errors');

//Handling user login
authRouter.post('/login', passport.authenticate('local', { session: false }), (req, res, next) => {
  const obj = createTokens(req.user); // Create token
  if (Object.keys(obj).length != 0) {
    res.cookie('nToken', obj.accessToken, { maxAge: 900000, httpOnly: true });
    res.redirect('/messages');
  }
});

//Showing login form
authRouter.get('/login', (req, res) => {
  res.render('login', { title: 'Login' });
});

//Handling user logout
authRouter.get('/logout', (req, res) => {
  res.clearCookie('nToken');
  return res.redirect('/auth/login');
});

// Handling user signup
authRouter.post('/register', upload.single('profile_pic'), async (req, res, next) => {
  try {

    const { username, password } = req.body;     // Capture the input fields

    // Ensure the input fields exists and are not empty
    if (!username) {
      throw new ValidationError('Username is required');
    } else if (!password) {
      throw new ValidationError('Password is required');
    } else if (password.length < 4) {
      throw new ValidationError('Invalid password');
    }
    const salt = await bcrypt.genSalt(10);   // generate salt to hash password

    const notExisting = await UserController.notExisting(req.body); //check if there is any existing user with same username
    if (notExisting) {

      //check if any profile image uploaded
      if (req.file) {
        req.body.profile_pic = req.file.filename;
      } else {
        req.body.profile_pic = '';
      }
      try {
        let password = req.body.password;
        password = await bcrypt.hash(password, salt);   // now we set user password to hashed password
        var userData = {
          username: req.body.username,
          password: password,
          name: req.body.name,
          profile_pic: req.body.profile_pic,
        };
        const user = await UserController.register(userData); // Save User to Database
        if (user) {
          const obj = createTokens(user); //create access token and refresh token for storing in cookies
          res.cookie('nToken', obj.accessToken, {
            maxAge: 900000,
            httpOnly: true,
          });

          res.redirect('/auth/login');  // Redirect to login page
        }
      } catch (err) {
        next(err);
      }
    }
  } catch (err) {
    next(err);
  }
});
module.exports = { authRouter, verifyToken };
