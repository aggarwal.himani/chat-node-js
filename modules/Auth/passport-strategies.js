const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const { UserModel } = require('../../models');
const { AuthError } = require('../../utils/errors');
const bcrypt = require('bcryptjs');

passport.use(
  new LocalStrategy(async (username, password, done) => {
    const user = await UserModel.findOne({ username: username }); // Validate if user exist in our database
    if (user) {
      const validPassword = await bcrypt.compare(password, user.password);  // check user password with hashed password stored in the database
      if (validPassword) {
        done(null, user._doc);
      } else {
        done(new AuthError('Invalid Password'), null);
      }
    } else {
      done(new AuthError('User not found'), null);
    }
  })
);
