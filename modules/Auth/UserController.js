const { UserModel } = require('../../models');
const { AuthError, SystemError, ValidationError } = require('../../utils/errors');
require('./passport-strategies');

class UserController {

  // Validate if user exist in our database
  async notExisting(userData) {
    const existUsername = await UserModel.findOne({
      username: userData.username,
    });
    if (existUsername) {
      throw new ValidationError('username taken');
    }
    return true;
  }

  async register(userData) {
    var newUser = new UserModel({
      username: userData.username,
      password: userData.password,
      name: userData.name,
      profile_pic: userData.profile_pic,
    });

    let saveUser = await newUser.save();
    return saveUser;
  }
}
module.exports = new UserController();
