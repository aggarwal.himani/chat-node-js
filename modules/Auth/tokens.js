const jwt = require('jsonwebtoken');
const { AuthError } = require('../../utils/errors');
const accessExp = '1h';
const refreshExp = '10d';
const PRIVATE_KEY = 'SOME_RANDOM_KEY';

function createTokens(user) {
  const accessToken = jwt.sign({ ...user, type: 'accesstoken' }, PRIVATE_KEY, {
    expiresIn: accessExp,
  });
  const refreshToken = jwt.sign({ ...user, type: 'refreshtoken' }, PRIVATE_KEY, { expiresIn: refreshExp });
  return {
    accessToken,
    refreshToken,
  };
}

function verifyToken(token) {
  try {
    const userdata = jwt.verify(token, PRIVATE_KEY);
    if (userdata.type == 'refreshToken') {
      throw new AuthError('refresh token provided');
    }
    return userdata;
  } catch (err) {
    throw new AuthError('Not authorized');
  }
}

module.exports = {
  createTokens,
  verifyToken,
};
