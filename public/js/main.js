var socket = io();
const inputField = document.querySelector('.message_form__input');
const inputFile = document.querySelector('.file_input');
const messageBox = document.querySelector('.messages__history ul');
const fallback = document.querySelector('.fallback');
var button = document.getElementById('submitbutton');
const files = document.getElementById('files');

let userName = document.querySelector('.senderName').value;

//send a new message
const addNewMessage = ({ user, message, media }) => {
  const formattedTime = new Date();

  const receivedMsg = `
    <li>
    <h2>${message}</h2>
    ${media}
    <p>${formattedTime} </p>
    </li>
    <hr />`;

  const myMsg = `
   <li>
    <h2>${message}</h2>
     ${media}
      <p>${formattedTime}</p>
    </li>
    <hr />`;

  messageBox.innerHTML += user === userName ? myMsg : receivedMsg;
  fallback.innerHTML = '';
};

//send button logic
button.addEventListener('click', function (e) {
  e.preventDefault();
  if (inputField.value == '' && inputFile.value == '') {
    return;
  }
  const messageContent = document.theForm.messageContent.value;
  const senderId = document.theForm.senderId.value;

  let formData = new FormData();
  for (let i = 0; i < files.files.length; i++) {
    formData.append('media', files.files[i]);
  }

  formData.append('messageContent', messageContent);
  formData.append('senderId', senderId);

  fetch('/messages/post', {
    method: 'POST',
    body: formData,
  })
    .then((response) => response.text())
    .then((data) => {
      console.log(data);
      var mediaHtml = '';
      if (data) {
        data = JSON.parse(data);
        if (data.media) {
          data.media.forEach((element) => {
            mediaHtml += '<a href="/data/uploads/' + element.media + '">' + element.media + '</a><br/>';
          });
        }
        socket.emit('chat message', {
          message: inputField.value,
          media: mediaHtml,
          nick: userName,
        });
        inputField.value = '';
        inputFile.value = '';
      }
    })
    .catch((error) => {
      console.log(error);
    });
});

inputField.addEventListener('keyup', () => {
  socket.emit('typing', {
    isTyping: inputField.value.length > 0,
    nick: userName,
  });
});

socket.on('chat message', function (data) {
  addNewMessage({ user: data.nick, message: data.message, media: data.media });
});

socket.on('typing', function (data) {
  const { isTyping, nick } = data;

  if (!isTyping) {
    fallback.innerHTML = '';
    return;
  }

  fallback.innerHTML = `<p>${nick} is typing...</p>`;
});
